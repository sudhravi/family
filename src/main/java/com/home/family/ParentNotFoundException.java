package com.home.family;

public class ParentNotFoundException extends RuntimeException {

	public ParentNotFoundException(String exception) {
		super(exception);
	}

}