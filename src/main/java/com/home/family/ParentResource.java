package com.home.family;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParentResource {
@Autowired
private ParentRepository parentRepo;
@GetMapping("/family")
public List<Parent> retrieveAll(){
	return parentRepo.findAll();
}
@GetMapping("/family/{id}")
public  Parent retrieveFamily(@PathVariable int id) {
	Optional<Parent> parent= parentRepo.findById(id);
	if(!parent.isPresent()) {
		throw new ParentNotFoundException("Id "+id);
	}
	return parent.get();
}
@DeleteMapping("/family/{id}")
public void deleteParent(@PathVariable int id) {
	parentRepo.deleteById(id);
}
@PutMapping("/family/{id}")
public Parent updateParent(@RequestBody Parent parent,@PathVariable int id) {
	Optional<Parent> parentold=parentRepo.findById(id);
	if(!parentold.isPresent()) {
		throw new ParentNotFoundException("Id "+id);
	}
	parent.setId(id);
	return parentRepo.save(parent);
	
}
@PostMapping("/family")
public Parent createEmployee(@RequestBody Parent parent) {
	Parent savedEmployee = parentRepo.save(parent);

	

	return savedEmployee;

}
}
